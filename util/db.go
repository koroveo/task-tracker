package util

import (
	"database/sql"
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	"log"
	"net/url"
)

func init() {
	viper.SetConfigFile(`config.json`)

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
}

func getDbDsn() string {
	dbHost := viper.GetString(`database.host`)
	dbPort := viper.GetString(`database.port`)
	dbUser := viper.GetString(`database.user`)
	dbPass := viper.GetString(`database.pass`)
	dbName := viper.GetString(`database.name`)

	dbUrl := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)

	dbAdditionalValues := url.Values{}
	dbAdditionalValues.Add("parseTime", "1")
	dbAdditionalValues.Add("loc", "Europe/Moscow")
	dbAdditionalValues.Add("multiStatements", "true")

	return fmt.Sprintf("%s?%s", dbUrl, dbAdditionalValues.Encode())
}

func ConnectToDbWithSql() *sql.DB {
	dbDsn := getDbDsn()

	db, err := sql.Open(`mysql`, dbDsn)
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	return db
}

func ConnectToDbWithOrm() *gorm.DB {
	dbDsn := getDbDsn()

	db, err := gorm.Open(`mysql`, dbDsn)
	if err != nil {
		log.Fatal(err)
	}

	err = db.DB().Ping()
	if err != nil {
		log.Fatal(err)
	}

	db.SingularTable(true)
	db.LogMode(true)

	return db
}

func MigrateDb() {
	dbSql := ConnectToDbWithSql()

	dbDriver, err := mysql.WithInstance(dbSql, &mysql.Config{})
	if err != nil {
		log.Fatal(err)
	}

	migration, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"mysql",
		dbDriver,
	)
	if err != nil {
		log.Fatal(err)
	}

	if err := migration.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatal(err)
	}

	err = dbSql.Close()
	if err != nil {
		log.Fatal(err)
	}
}
