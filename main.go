package main

import (
	"./controller/sign_in"
	"./controller/sign_up"
	//"./model"
	"./util"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
	"log"
	"net/http"
)

func main() {
	util.MigrateDb()

	db := util.ConnectToDbWithOrm()
	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	router := mux.NewRouter()

	router.HandleFunc("/sign_up", sign_up.Index).Methods("GET")
	router.HandleFunc("/sign_up", sign_up.SignUp).Methods("POST")

	router.HandleFunc("/sign_in", sign_in.Index).Methods("GET")
	router.HandleFunc("/sign_in", sign_in.SignIn).Methods("POST")

	http.Handle("/", router)

	log.Fatal(http.ListenAndServe(viper.GetString("server.address"), nil))
}
