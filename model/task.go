package model

type Task struct {
	Common
	Title  string `gorm:"type:text;not_null"`
	UserID uint   `gorm:"type:text;not_null"`
}
