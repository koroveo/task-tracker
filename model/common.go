package model

import "time"

type Common struct {
	ID          uint      `gorm:"type:int;not_null;auto_increment;unique_index;primary_key"`
	IsActive    bool      `gorm:"type:bool;not_null;default:true"`
	DateCreated time.Time `gorm:"type:datetime;not_null;default:now()"`
	DateUpdated time.Time `gorm:"type:datetime;not_null;default:now()"`
}
