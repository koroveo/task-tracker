package model

import "github.com/jinzhu/gorm"

type User struct {
	Common
	Email    string `gorm:"type:text;not_null;unique_index"`
	Password string `gorm:"type:text;not_null"`
	Tasks    []Task `gorm:"foreignkey:Task"`
}

func FindUserByEmail(db *gorm.DB, email string) User {
	userStruct := User{}

	db.Where("email = ?", email).First(&userStruct)

	return userStruct
}

func CreateUser(db *gorm.DB, email, password string) {
	userStruct := User{
		Email:    email,
		Password: password,
	}

	db.Create(&userStruct)
}
