package sign_in

import (
	"../../model"
	"../../util"
	"github.com/blue-jay/core/passhash"
	"html/template"
	"log"
	"net/http"
)

var db = util.ConnectToDbWithOrm()

func Index(response http.ResponseWriter, request *http.Request) {
	page := template.Must(template.ParseFiles("template/sign_in.html"))

	err := page.Execute(response, nil)
	if err != nil {
		log.Fatal(err)
	}
}
func SignIn(response http.ResponseWriter, request *http.Request) {
	email := request.FormValue("email")
	password := request.FormValue("password")

	user := model.FindUserByEmail(db, email)

	if user.Email == "" {
		http.Redirect(response, request, "/sign_in", http.StatusFound)
	} else if passhash.MatchString(password, user.Password) {

	} else {
		log.Fatal(user.Email)
	}

	http.Redirect(response, request, "/", http.StatusFound)
}