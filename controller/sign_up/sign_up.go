package sign_up

import (
	"../../model"
	"../../util"
	"github.com/blue-jay/core/passhash"
	"html/template"
	"log"
	"net/http"
)

var db = util.ConnectToDbWithOrm()

func Index(response http.ResponseWriter, request *http.Request) {
	page := template.Must(template.ParseFiles("template/sign_up.html"))

	err := page.Execute(response, nil)
	if err != nil {
		log.Fatal(err)
	}
}
func SignUp(response http.ResponseWriter, request *http.Request) {
	email := request.FormValue("email")
	password, err := passhash.HashString(request.FormValue("password"))
	if err != nil {
		log.Fatal(err)
	}

	user := model.FindUserByEmail(db, email)

	if user.Email == "" {
		model.CreateUser(db, email, password)
	} else {
		log.Fatal(user.Email)
	}

	http.Redirect(response, request, "/sign_in", http.StatusFound)
}