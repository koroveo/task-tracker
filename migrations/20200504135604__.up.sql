create table user
(
    id int not null,
    is_active bool not null default 1,
    date_created datetime not null default now(),
    date_updated datetime not null default now(),
    email varchar(255) not null,
    password varchar(255) not null
);

create unique index user_email_uindex
    on user (email);

create unique index user_id_uindex
    on user (id);

alter table user
    add constraint user_pk
        primary key (id);

alter table user modify id int auto_increment;

create table task
(
    id int not null,
    is_active bool not null default 1,
    date_created datetime not null default now(),
    date_updated datetime not null default now(),
    title varchar(255) not null,
    user_id int not null,
    constraint task_user_id_fk
        foreign key (user_id) references user (id)
);

create unique index task_id_uindex
    on task (id);

alter table task
    add constraint task_pk
        primary key (id);

alter table task modify id int auto_increment;

